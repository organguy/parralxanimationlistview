package or.kr.parralaxanimationlistview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.poliveira.apps.parallaxlistview.ParallaxListView;
import com.poliveira.apps.parallaxlistview.ParallaxScrollEvent;

import java.util.ArrayList;
import java.util.List;

public class ParralaxListActivity extends AppCompatActivity {

    ArrayAdapter<String> mAdapter;
    RelativeLayout mContainer;
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parralax_list_view);

        List<String> mStrings = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            mStrings.add("Android String " + i);
        }
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mStrings);


        mContainer = (RelativeLayout) findViewById(R.id.rl_container);
        View v = getLayoutInflater().inflate(R.layout.include_listview, mContainer, true);
        ParallaxListView plvList = (ParallaxListView) v.findViewById(R.id.plv_list);
        plvList.setAdapter(mAdapter);

        plvList.setParallaxView(getLayoutInflater().inflate(R.layout.view_header, plvList, false));


    }

    private void initView(){
        View v = getLayoutInflater().inflate(R.layout.include_listview, mContainer, true);
        ParallaxListView plvList = (ParallaxListView) v.findViewById(R.id.plv_list);
        plvList.setAdapter(mAdapter);

        plvList.setParallaxView(getLayoutInflater().inflate(R.layout.view_header, plvList, false));

        /*mImageView = new ImageView(this);

        final int size = Math.round(48 * getResources().getDisplayMetrics().density);
        final int buttonsSize = findViewById(R.id.linearLayout).getHeight();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size, size);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        params.setMargins(0, 0, Math.round(16 * getResources().getDisplayMetrics().density), 0);
        mImageView.setBackgroundResource(R.drawable.floating_button);
        mImageView.setImageResource(R.drawable.ic_action_add);
        plvList.setScrollEvent(new ParallaxScrollEvent() {
            @Override
            public void onScroll(double percentage, double offset, View parallaxView) {
                double translation = parallaxView.getHeight() - (parallaxView.getHeight() * percentage) + size / 2 - buttonsSize;
                ViewCompat.setTranslationY(mImageView, (float) translation);
            }
        });*/
    }
}