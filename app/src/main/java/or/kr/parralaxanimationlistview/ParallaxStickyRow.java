package or.kr.parralaxanimationlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;



/**
 * 공지사항 리스트 ROW
 */
public class ParallaxStickyRow extends FrameLayout{

   public ParallaxStickyRow(Context context) {
        super(context);
       LayoutInflater.from(context).inflate(R.layout.sticky_row, this);
    }
}
