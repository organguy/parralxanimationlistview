package or.kr.parralaxanimationlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ParallaxStickyHeaderActivity extends AppCompatActivity implements AbsListView.OnScrollListener {

    private ImageView stickyView;
    private ListView listView;
    private View heroImageView;

    private View stickyViewSpacer;

    private int MAX_ROWS = 13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parallax_sticky_header);

        getSupportActionBar().hide();

        /* Initialise list view, hero image, and sticky view */
        listView = (ListView) findViewById(R.id.listView);
        heroImageView = findViewById(R.id.heroImageView);
        stickyView = (ImageView) findViewById(R.id.stickyView);

        /* Inflate list header layout */
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflater.inflate(R.layout.sticky_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);

        /* Add list view header */
        listView.addHeaderView(listHeader);

        listView.setOnScrollListener(this);

        /* Populate the ListView with sample data */
        List<String> modelList = new ArrayList<>();
        for (int i = 0; i < MAX_ROWS; i++) {
            modelList.add("List item " + i);
        }

        ParallaxStickyAdapter adapter = new ParallaxStickyAdapter(this, modelList);
        listView.setAdapter(adapter);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        /* Check if the first item is already reached to top.*/
        if (listView.getFirstVisiblePosition() == 0) {
            View firstChild = listView.getChildAt(0);
            int topY = 0;
            if (firstChild != null) {
                topY = firstChild.getTop();
            }

            int heroTopY = stickyViewSpacer.getTop();
            stickyView.setY(Math.max(0, heroTopY + topY));

            /* Set the image to scroll half of the amount that of ListView */
            heroImageView.setY(topY * 0.5f);
        }
    }


}