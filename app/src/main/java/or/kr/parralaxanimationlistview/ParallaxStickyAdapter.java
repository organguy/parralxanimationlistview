package or.kr.parralaxanimationlistview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;


/**
 * 공지사항 리스트 아답터
 */

public class ParallaxStickyAdapter extends BaseAdapter{
    Context mContext;
    List<String> list;
    public ParallaxStickyAdapter(Context _context, List<String> list) {
        mContext = _context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ParallaxStickyRow itemView;
        if (convertView == null) {
            itemView = new ParallaxStickyRow(mContext);
        } else {
            itemView = (ParallaxStickyRow)convertView;
        }

        return itemView;
    }
}
