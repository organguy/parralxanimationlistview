package or.kr.parralaxanimationlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickExample(View view) {

        Intent intent = null;

        switch (view.getId()){
            case R.id.bt_parralx_list:
                intent = new Intent(MainActivity.this, ParralaxListActivity.class);
                break;

            case R.id.bt_parralx_sticky:
                intent = new Intent(MainActivity.this, ParallaxStickyHeaderActivity.class);
                break;
        }

        startActivity(intent);
    }
}